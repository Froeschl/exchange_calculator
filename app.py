from flask import Flask
from flask_restx import Resource, Api, fields
from converter_functions import convert, get_exchange_rate, ApiError

app = Flask(__name__)

api = Api(app,
          version='1.0.0',
          title='ExchangeCalculator API',
          description='ExchangeCalculator API',
          default="ExchangeCalculator API",
          default_label=""
          )

example_data = [
    34.5677,
    34.57,
    34.57,
    2,
    1
]

exchange_rq = api.model('ExchangeRQ', {
    "rates": fields.List(fields.Float,
                         required=True,
                         description="Requests to compute exchanged rates",
                         default=example_data)
})
exchange_rs = api.model("ExchangeRS", {
    "rates": fields.List(fields.Float,
                         required=True,
                         description="Success. Array of converted rates.")
})


@api.route('/exchange/')
class CurrencyConverter(Resource):
    @api.expect(exchange_rq, validate=True)
    def post(self):
        """Returns a list of converted values from the input list"""
        values = api.payload["rates"]

        input_currency = api.payload.get("input_currency", "EUR")
        output_currency = api.payload.get("output_currency", "USD")

        try:
            exchange_rate = get_exchange_rate(input_currency, output_currency)
        except (ApiError, ValueError) as e:
            return {
                "success": False,
                "message": str(e),
            }

        results = [convert(value, exchange_rate) for value in values if value != ""]

        response = {
            "success": True,
            "rates": results,
            "input_currency": input_currency,
            "output_currency": output_currency
        }
        return response


if __name__ == '__main__':
    app.run(debug=True)
