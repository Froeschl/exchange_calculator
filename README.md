# Currency Converter

## Getting started

To run the project on Windows please use the following commands:
```
git clone https://gitlab.com/Froeschl/exchange_calculator.git
cd exchange_calculator
python -m venv venv
.\venv\scripts\activate
pip install -r requirements.txt
python -m flask run
```
**Attention:** The API_KEY is for security reasons not part of the code. In order to make API calls please create a copy of the .env-example file, rename it to .env and add your own API-key for fixer.io

## Usage
- To make use of the API send `"rates": [34.5677, 34.57, 34.57, 1]` in the request body to the endpoint `http://127.0.0.1:5000/exchange/` **OR**
- Use the swagger documentation

## Links
* Swagger documentation: [http://127.0.0.1:5000/](http://127.0.0.1:5000/)
* Exchange Endpoint: [http://127.0.0.1:5000/exchange/](http://127.0.0.1:5000/exchange/)


## Example request body default
```json
{
    "rates": [
		34.5677, 
		34.57, 
		34.57, 
		1
	]
}
```

## Example request body EXTENDED
```json
{
    "rates": [
		34.5677, 
		34.57, 
		34.57, 
		1
	],
    "input_currency": "EUR",
    "output_currency": "CHF"
}
```

## Example response body
```json
{
    "success": true,
    "rates": [
        40.5588354932,
        40.561534120000005,
        40.561534120000005,
        1.173316
    ],
    "input_currency": "EUR",
    "output_currency": "USD"
}
```

## Design Decision
The app is made with flask and has one POST endpoint. 
The two functions which are needed for the calculation are in a separated file, which makes the code easier to read and maintain.
Per default the app converts from EUR to USD. If `"input_currency": "USD"` and `"output_currency": "USD"` are added in the request body this behaviour is overwritten.

## Scalability
There are basically to options how to scale this app:
* Vertical scaling
* Horizontal scaling

Vertical scaling can be achieved by scaling the server. The use of Gunicorn webserver makes it easy to add additional workers and also includes load balancing within multiple instances.

A horizontal scaling approach would be, for example, the use of AWS Elastic Beanstalk. Here AWS takes care of a lot of different tasks like server setup, load balancing, auto-scaling, etc. This variant is easy to maintain and very scalable.

## Performance
Currently, the app is configured to make an API call to fixer.io every time a request is done; however, the rate is updated once an hour only (based on the subscription model). To increase the performance the exchange_rate could be stored in cache. That would reduce the response time of the app.
Alternatively with the package flask-apscheduler (https://github.com/viniciuschiele/flask-apscheduler) a cronjob could be set up and executed based on the subscription model.

## Security
* For calling the external API an API-Key is needed. Due to best practice this should not be stored in the VCS. That is the reason why I have decided to make use of python-dotenv and an .env-file.
* HTTP Requests for fixer.io: The free plan only offers http-requests and no https-requests. 
* In production an authentication is needed to protect this app from abuse.

## Used Packages
* flask: is a lightweight python web framework. perfect for this task
* requests: is needed to retrieve data from the external api
* flask-restx: is needed to build easily REST APIs and also it includes the support for swagger
* python-dotenv: is needed here to make use on an .env file. In this case the file stores the API key. The .env file is mentioned in the .gitignore file
