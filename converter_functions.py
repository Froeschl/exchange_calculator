import os

import requests
from dotenv import load_dotenv
load_dotenv()

api_key = os.getenv("API_KEY")
base_url = "http://data.fixer.io/api/latest"


class ApiError(Exception):
    pass


def convert(amount, change_rate):
    """Multiplies input amount with exchange rate

    Rounding of the float change be added here if needed
    """
    return amount * change_rate


def get_exchange_rate(input_currency, output_currency):
    """Calls an api to get exchange rate for currencies

    If external api call is not successful an ApiError is raised
    If input or output_currency are not available a ValueError is raised
    """
    CURRENCIES = ["USD", "EUR", "CHF", "GBP"]
    if input_currency not in CURRENCIES or output_currency not in CURRENCIES:
        currencies_available = ", ".join(CURRENCIES)
        raise ValueError(
            f"The currency you have chosen is not available. "
            f"Currently available are: {currencies_available}"
        )

    url = base_url + f"?access_key={api_key}" \
                     f"&base={input_currency}" \
                     f"&symbols={output_currency}"

    response = requests.get(
        url
    )
    if response.status_code != 200:
        raise ApiError(
            f"External API not available. Status code: {response.status_code}"
        )

    result = response.json()
    if not result["success"]:
        error_code = result["error"]["code"]
        error_type = result["error"]["type"]
        raise ApiError(
            f"Error code: {error_code} - Error type: {error_type}"
        )
    return result["rates"][output_currency]
